import requests as rq
import random
DEBUG = False

class WBot:
    words = [word.strip() for word in open("words.txt")]
    mm_url = "https://we6.talentsprint.com/wordle/game/"
    register_url = mm_url + "register"
    creat_url = mm_url + "create"
    guess_url = mm_url + "guess"


    '''def __init__(self, name: str):
        def is_unique(w: str) -> bool:
            return len(w) == len(set(w))'''

        self.session = rq.session()
        register_dict = {"mode": "wordle", "name": name}
        reg_resp = self.session.post(WBot.register_url, json=register_dict)

    def __init__(self, name: str):
        
        self.session = rq.session()
        register_dict = {"mode": "wordle", "name": name}
        reg_resp = self.session.post(WBot.register_url, json=register_dict)

        self.me = reg_resp.json()['id']
        creat_dict = {"id": self.me, "overwrite": True}
        self.session.post(WBot.creat_url, json=creat_dict)

        self.choices = WBot.words

        random.shuffle(self.choices)

    def play(self) -> str:
        def post(choice: str) -> tuple:
            guess = {"id": self.me, "guess": choice}
            response = self.session.post(WBot.guess_url, json=guess)
            rj = response.json()
            right = (rj["feedback"])
            status = rj["message"][0]
            return right, status

        choice = random.choice(self.choices)
        self.choices.remove(choice)
        colors , guess_left = post(choice)
        tries = [f'{choice}:{colors}']

        while status != 0:
            if DEBUG:
                print(choice, right, self.choices[:10])
            self.update(choice, right)
            choice = random.choice(self.choices)
            self.choices.remove(choice)
            colors, guess_left = post(choice)
            tries.append(f'{choice}:{colors}')
        print("Secret is", choice, "found in", len(tries), "attempts")
        print("Route is:", " => ".join(tries))

   ''' def update(self, choice: str, colors: int):
        def common(choice: str, word: str):
            return len(set(choice) & set(word))
        self.choices = [w for w in self.choices if common(choice, w) == right]

        def green(self, choice, letter):
            def match_pos(letter, choice, word):
                return choice.index(letter) == word.index(letter)
            self.choices = [w for w in self.choices if match_pos(letter, choice, w)]

        def red(self: Self, choice: int, colors: str):
            words_to_remove = [word for index, color in enumerate(colors) if color == 'R' for word in choices[index]]
            [self.choices.remove(word) for word in words_to_remove if word in self.choices]
        
        def yellow(self: Self, choice: int, colors: str):
            words_to_keep = [word for index , color in enumerate(colors) if color == "Y"  for word in choices if choice[index] in word]
            [self.choices.remove(word) for word not in words_to_keep ]'''
            def update(self, choice: str, feedback: str):
        self.choices = [w for w in self.choices if self.match(choice,w, feedback)]

    def match(self, choice: str, word: str, feedback: str) -> bool:
        for i in range(5):
            if feedback[i] == 'G':
                if choice[i]!= word[i]:
                    return False
            elif feedback[i] == 'Y':
                if choice[i] not in word:
                    return False
            elif feedback[i] == 'R':
                if choice[i] in word:
                    return False
        return True
                

game = WBot("CodeShifu")
game.play()


